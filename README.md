# Springboot OpenAPI with Swagger
## 프로젝트 안내
Springboot에서 OpenAPI Specification 3.0 연동 및 구성    
### 개발환경
Java 11, Gradle 6.5 이상 

### 프로젝트 구성
springboot 2.4.x, springdoc-openapi, lombok 
* 테스트 URL  
http://localhost:9082/swagger-ui/

### 소스 구성 설명
step-10 ~ step-40까지 순차적으로 코드가 구체화 되며 각 스텝별로 테스트 할 수 있음

### 참고
* https://blog.jiniworld.me/83?category=850715
* https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Integration-and-configuration
* https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
* https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md
* https://javadoc.io/doc/io.swagger.core.v3/swagger-annotations/latest/index.html
