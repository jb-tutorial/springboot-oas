package com.example.oas.config;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.stereotype.Component;

/**
 * https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Integration-and-configuration
 */
@OpenAPIDefinition(
        info = @Info(
                title = "Sample API",
                version = "0.1",
                description = "Springboot OAS along Swagger UI",
                license = @License(name = "Apache 2.0", url = "http://foo.bar"),
                contact = @Contact(url = "http://www.ubpay.com",
                        name = "UBpay",
                        email = "jangb@ubpay.com")
        ),
//        tags = {
//                @Tag(name = "Tag 1", description = "desc 1", externalDocs = @ExternalDocumentation(description = "docs desc")),
//                @Tag(name = "Tag 2", description = "desc 2", externalDocs = @ExternalDocumentation(description = "docs desc 2")),
//                @Tag(name = "Tag 3")
//        },
        externalDocs = @ExternalDocumentation(description = "참고문서", url = "https://www.naver.com")
)
@SecurityScheme(name = "UBpay Internal BearerAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "bearer",
        bearerFormat = "JWT")
@Component
public class OasConfig {
//    @Bean
//    public OpenAPI openAPI () {
//        Info info = new Info()
//                .title("Sample API")
//                .description("Springboot OAS along Swagger UI")
//                .version("1.0")
//                .contact(new Contact().email("jangb@ubpay.com"));
//
//        return new OpenAPI()
//                .components(new Components())
//                .info(info);
//    }
}
