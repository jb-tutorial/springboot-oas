package com.example.oas.controller;

import com.example.oas.controller.model.ErrorResponse;
import com.example.oas.controller.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "연습용 컨트롤러", description = "컨트롤러 상세설명")
@RestController
@RequestMapping("/sample")
public class HelloController {

    @Operation(summary = "사용자 확인", description = "사용자 이름을 path애서 가져옵니다")
    @GetMapping("/{name}")
    public ResponseEntity<?> checkUser(
            @Parameter(description = "사용자이름", example = "장비")
            @PathVariable String name) {

        return ResponseEntity.ok("Hello " + name);
    }

    @Operation(summary = "사용자 정보갱신", description = "사용자 정보를 업데이트 합니다")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = User.class))),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PostMapping("/user")
    public ResponseEntity<?> updateUser(
//            @Parameter(schema = @Schema(implementation = User.class))
            @RequestBody User user) {

        return ResponseEntity.ok(
                User.builder()
                        .id(user.getId())
                        .name(user.getName())
                        .email(user.getEmail())
                        .userType(user.getUserType())
                        .build());
    }}
