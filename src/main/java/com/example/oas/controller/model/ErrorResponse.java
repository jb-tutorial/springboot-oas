package com.example.oas.controller.model;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "공통 에러응답")
public class ErrorResponse {
    @Schema(title = "에러코드", defaultValue = "에러코", required = true, example = "0",
            externalDocs = @ExternalDocumentation(description = "에러코드 정리문서", url = "https://www.naver.com"))
    Long code;

    @Schema(title = "에러 메시지", defaultValue = "에러코드", required = true)
    String message;

    @Schema(title = "에러 메시지", defaultValue = "에러코", hidden = true)
    String internalErrorCode;
}
