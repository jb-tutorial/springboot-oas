package com.example.oas.controller.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@Schema(description = "사용자정보 요청 및 응답")
public class User {
    @Schema(title = "사용자 ID", defaultValue = "여기저기 쓰이니까 꼭 필요함", required = true, example = "100")
    @Size(min = 1)
    Long id;

    @Schema(title = "사용자 이름", defaultValue = "장비", example = "장비")
    @NotEmpty
    String name;

    @Schema(title = "email", example = "jangb@ubpay.com")
    @Email
    String email;

    @Schema(title = "사용자 구분", allowableValues = {"user", "member", "admin"}, example = "user")
    String userType;
}
